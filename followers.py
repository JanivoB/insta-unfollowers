import instaloader
import pandas as pd
import os

username = ""
psw = ""



bot = instaloader.Instaloader()
profile = instaloader.Profile.from_username(bot.context, username)
bot.login(user=username, passwd=psw)

def get_my_followers():
    os.remove("followers.txt")
    profile = instaloader.Profile.from_username(bot.context, username)
    bot.login(user=username, passwd=psw)
    print("Writing followers...")
    try:
        for followers in profile.get_followers():
            with open("followers.txt","a+") as f:
                file = f.write(followers.username+'\n')
            
    except:
        print("Failed at: get_my_followers")

def get_my_followees():
    os.remove("followees.txt")
    profile = instaloader.Profile.from_username(bot.context, username)
    bot.login(user=username, passwd=psw)
    print("Writing followees...")
    try:
        for followees in profile.get_followees():
            with open("followees.txt","a+") as f:
                file = f.write(followees.username+'\n')
    except:
        print("Failed at: get_my_followees")

def get_my_unfollowers():
    os.remove("unfollowers_set.txt")
    profile = instaloader.Profile.from_username(bot.context, username)
    bot.login(user=username, passwd=psw)
    print("Writing unfollowers...")
    try:
        followers_file = set(open("followers.txt").readlines())
        followees_file = set(open("followees.txt").readlines())

        unfollowers_set = followees_file.difference(followers_file)
            
        for unfollowers in unfollowers_set:
            with open("unfollowers_set.txt","a+") as f:
                file = f.write(unfollowers)
    except:
        print("Failed at: get_my_unfollowers")

print("username: ", profile.username)
print("Followers Count: ", profile.followers)
print("Following Count: ", profile.followees)
print("Number of Posts: ", profile.mediacount)

get_my_followers()
get_my_followees()
get_my_unfollowers()